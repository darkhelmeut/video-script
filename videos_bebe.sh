#!/bin/bash

CONVERTER='youtube-dl'
GETFILENAME='youtube-dl --get-filename '


if ! type ${CONVERTER} > /dev/null ;
then
	echo "La commande ${CONVERTER} doit être installé"
	exit 1
fi


EXPORT_DIR='./videos_bebe'
if [ ! -d ${EXPORT_DIR} ];
then
	mkdir ${EXPORT_DIR}
fi

exit;

echo "Merci de fournir le fichier des liste de videos"
read SOURCE_FILE

echo "Vous avez selectionné le fichier ${SOURCE_FILE}"

if [ ! -f ./${SOURCE_FILE} ];
then
	echo "Le fichier n'existe pas"
	exit 1
fi

COUNT_YOUTUBE_VIDEOS=`cat ${SOURCE_FILE} | grep -i "youtube.com" | wc -l`

echo "${COUNT_YOUTUBE_VIDEOS} vont être convertis"

LIST_YOUTUBE_URLS=`cat ${SOURCE_FILE} | grep -i "youtube.com"`


for YOUTUBE_URL in ${LIST_YOUTUBE_URLS}; 
do
	echo ${YOUTUBE_URL}
	FILE_EXPORT_NAME=`${GETFILENAME} ${YOUTUBE_URL}`
	echo ${EXPORT_DIR}/${FILE_EXPORT_NAME}
	if [ ! -f "${EXPORT_DIR}/${FILE_EXPORT_NAME}" ]; 
	then
		${CONVERTER} ${YOUTUBE_URL} ${EXPORT_DIR}
	else 
		echo "fichier existant"
	fi
	
done
